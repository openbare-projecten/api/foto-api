FROM registry.gitlab.com/openbare-projecten/oracle-jdk-8-raspi-docker

RUN apt-get update && apt-get install git -y

COPY target/foto-api-1.0.0-SNAPSHOT.jar foto-api-1.0.0-SNAPSHOT.jar

ENTRYPOINT ["java", "-jar", "foto-api-1.0.0-SNAPSHOT.jar"]