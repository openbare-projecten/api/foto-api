package nl.robertvankammen.fotoapi;

import nl.robertvankammen.fotoapi.model.Afbeelding;
import nl.robertvankammen.fotoapi.model.Persoon;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Random;

public final class ObjectBuilder {

    public static Afbeelding newAfbeelding(Persoon persoon) {
        final Afbeelding afbeelding = new Afbeelding();
        afbeelding.setId(new Random().nextLong());
        afbeelding.setUrn("urn:jpg:" + RandomStringUtils.randomAlphabetic(10));
        afbeelding.setLaatstGewijzigdDatumTijd(LocalDateTime.now());
        afbeelding.setOrgineleDatum(LocalDateTime.now());
        afbeelding.setPersonenLijst(Collections.singletonList(persoon));
        persoon.getAfbeeldingenLijst().add(afbeelding);
        return afbeelding;
    }

    public static Afbeelding newAfbeelding() {
        return newAfbeelding(newPersoon());
    }

    public static Persoon newPersoon() {
        Persoon persoon = new Persoon();
        persoon.setNaam(RandomStringUtils.randomAlphabetic(10));
        persoon.setId(new Random().nextLong());
        persoon.setProfielFoto(false);
        return persoon;
    }
}
