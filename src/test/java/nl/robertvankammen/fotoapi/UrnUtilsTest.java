package nl.robertvankammen.fotoapi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class UrnUtilsTest {

    private final UrnUtils urnUtils = new UrnUtils();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testMaakPathVanUrn_enkeleFile() {
        final String urn = "urn:jpg:abc";

        assertThat(urnUtils.maakPathVanUrn(urn), is(File.separator + "abc.jpg"));
    }

    @Test
    public void testMaakPathVanUrn_meerdereDirectorys() {
        final String urn = "urn:jpg:abc:def:asd";

        assertThat(urnUtils.maakPathVanUrn(urn), is(File.separator + "abc/def/asd.jpg"));
    }

    @Test
    public void testMaakPathVanUrn_geenGeldigeUrn() {
        final String urn = "aaa:xxx:abc:def:asd";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(urn + " is geen geldige urn, deze moet bestaan uit het volgende urn:extention:subpath:etc:filename");


        urnUtils.maakPathVanUrn(urn);
    }

    @Test
    public void testMaakPathVanUrn_nietOndersteundeExtention() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("De volgende extentie wordt niet ondersteund: xxx");

        final String urn = "urn:xxx:abc:def:asd";

        urnUtils.maakPathVanUrn(urn);
    }

}