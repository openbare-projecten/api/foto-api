package nl.robertvankammen.fotoapi.controller;

import nl.robertvankammen.fotoapi.model.Afbeelding;
import nl.robertvankammen.fotoapi.model.Persoon;
import nl.robertvankammen.fotoapi.repository.AfbeeldingRepository;
import nl.robertvankammen.fotoapi.repository.PersoonRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

import static nl.robertvankammen.fotoapi.ObjectBuilder.newAfbeelding;
import static nl.robertvankammen.fotoapi.ObjectBuilder.newPersoon;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ZoekenControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private PersoonRepository persoonRepository;

    @Mock
    private AfbeeldingRepository afbeeldingRepository;

    @Mock
    private EntityManager entityManager;

    @Mock
    private Environment environment;

    @InjectMocks
    private ZoekenController zoekenController;

    @Test
    public void testZoeken_1persoon1Afbeelding() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding = newAfbeelding(persoon);

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));

        final HashMap<String, Set> returnValue = zoekenController.zoeken(persoon.getId().toString(), 1, 0, null);

        assertThat(returnValue.containsKey("namen"), is(true));
        assertThat(returnValue.containsKey("afbeeldingen"), is(true));

        final Set<Persoon> persoonSet = returnValue.get("namen");
        final Set<Afbeelding> afbeeldingenSet = returnValue.get("afbeeldingen");

        assertThat(persoonSet, hasSize(1));
        assertThat(persoonSet, hasItem(persoon));
        assertThat(afbeeldingenSet, hasSize(1));
        assertThat(afbeeldingenSet, hasItem(afbeelding));
    }

    @Test
    public void testZoeken_1Persoon2AfbeeldingenSortAsc() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding1 = newAfbeelding(persoon);
        final Afbeelding afbeelding2 = newAfbeelding(persoon);
        afbeelding1.setOrgineleDatum(LocalDateTime.now().minusDays(1));

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));

        final HashMap<String, Set> returnValue = zoekenController.zoeken(persoon.getId().toString(), 2, 0, "dateAsc");

        assertThat(returnValue.containsKey("namen"), is(true));
        assertThat(returnValue.containsKey("afbeeldingen"), is(true));

        final Set<Persoon> persoonSet = returnValue.get("namen");
        final Set<Afbeelding> afbeeldingenSet = returnValue.get("afbeeldingen");

        assertThat(persoonSet, hasSize(1));
        assertThat(persoonSet, hasItem(persoon));
        assertThat(afbeeldingenSet, hasSize(2));

        final Iterator<Afbeelding> afbeeldingIterator = afbeeldingenSet.iterator();

        assertThat(afbeeldingIterator.next(), is(afbeelding1));
        assertThat(afbeeldingIterator.next(), is(afbeelding2));
    }

    @Test
    public void testZoeken_1Persoon2AfbeeldingenSortDesc() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding1 = newAfbeelding(persoon);
        final Afbeelding afbeelding2 = newAfbeelding(persoon);
        afbeelding1.setOrgineleDatum(LocalDateTime.now().minusDays(1));

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));

        final HashMap<String, Set> returnValue = zoekenController.zoeken(persoon.getId().toString(), 2, 0, "dateDesc");

        assertThat(returnValue.containsKey("namen"), is(true));
        assertThat(returnValue.containsKey("afbeeldingen"), is(true));

        final Set<Persoon> persoonSet = returnValue.get("namen");
        final Set<Afbeelding> afbeeldingenSet = returnValue.get("afbeeldingen");

        assertThat(persoonSet, hasSize(1));
        assertThat(persoonSet, hasItem(persoon));
        assertThat(afbeeldingenSet, hasSize(2));

        final Iterator<Afbeelding> afbeeldingIterator = afbeeldingenSet.iterator();

        assertThat(afbeeldingIterator.next(), is(afbeelding2));
        assertThat(afbeeldingIterator.next(), is(afbeelding1));
    }

    @Test
    public void testZoeken_2personen2AfbeeldingenPerPersoonSortAsc() {
        final Persoon persoon1 = newPersoon();
        final Persoon persoon2 = newPersoon();
        final Afbeelding afbeelding1 = newAfbeelding(persoon1);
        final Afbeelding afbeelding2 = newAfbeelding(persoon1);
        final Afbeelding afbeelding3 = newAfbeelding(persoon2);
        final Afbeelding afbeelding4 = newAfbeelding(persoon2);
        final Query mockQuery = mock(Query.class);
        afbeelding1.setOrgineleDatum(LocalDateTime.now().minusDays(10));
        afbeelding2.setOrgineleDatum(LocalDateTime.now().minusDays(80));
        afbeelding3.setOrgineleDatum(LocalDateTime.now().minusDays(1));
        afbeelding4.setOrgineleDatum(LocalDateTime.now().minusDays(99));

        given(persoonRepository.findById(persoon1.getId())).willReturn(Optional.of(persoon1));
        given(persoonRepository.findById(persoon2.getId())).willReturn(Optional.of(persoon2));
        given(afbeeldingRepository.findById(afbeelding1.getId())).willReturn(Optional.of(afbeelding1));
        given(afbeeldingRepository.findById(afbeelding2.getId())).willReturn(Optional.of(afbeelding2));
        given(afbeeldingRepository.findById(afbeelding3.getId())).willReturn(Optional.of(afbeelding3));
        given(afbeeldingRepository.findById(afbeelding4.getId())).willReturn(Optional.of(afbeelding4));

        given(entityManager.createNativeQuery(anyString())).willReturn(mockQuery);
        given(mockQuery.getResultList()).willReturn(Arrays.asList(
                BigInteger.valueOf(afbeelding1.getId()), BigInteger.valueOf(afbeelding2.getId()),
                BigInteger.valueOf(afbeelding3.getId()), BigInteger.valueOf(afbeelding4.getId())));

        final HashMap<String, Set> returnValue = zoekenController.zoeken(persoon1.getId() + ";" + persoon2.getId(), 4, 0, "dateAsc");

        assertThat(returnValue.containsKey("namen"), is(true));
        assertThat(returnValue.containsKey("afbeeldingen"), is(true));

        final Set<Persoon> persoonSet = returnValue.get("namen");
        final Set<Afbeelding> afbeeldingenSet = returnValue.get("afbeeldingen");

        assertThat(persoonSet, hasSize(2));
        assertThat(persoonSet, hasItem(persoon1));
        assertThat(persoonSet, hasItem(persoon2));
        assertThat(afbeeldingenSet, hasSize(4));

        final Iterator<Afbeelding> afbeeldingIterator = afbeeldingenSet.iterator();

        assertThat(afbeeldingIterator.next(), is(afbeelding4));
        assertThat(afbeeldingIterator.next(), is(afbeelding2));
        assertThat(afbeeldingIterator.next(), is(afbeelding1));
        assertThat(afbeeldingIterator.next(), is(afbeelding3));
    }

    @Test
    public void testZoeken_2personen2AfbeeldingenPerPersoonSortAscCache() {
        final Persoon persoon1 = newPersoon();
        final Persoon persoon2 = newPersoon();
        final Afbeelding afbeelding1 = newAfbeelding(persoon1);
        final Afbeelding afbeelding2 = newAfbeelding(persoon1);
        final Afbeelding afbeelding3 = newAfbeelding(persoon2);
        final Afbeelding afbeelding4 = newAfbeelding(persoon2);
        final Query mockQuery = mock(Query.class);
        afbeelding1.setOrgineleDatum(LocalDateTime.now().minusDays(10));
        afbeelding2.setOrgineleDatum(LocalDateTime.now().minusDays(80));
        afbeelding3.setOrgineleDatum(LocalDateTime.now().minusDays(1));
        afbeelding4.setOrgineleDatum(LocalDateTime.now().minusDays(99));

        given(persoonRepository.findById(persoon1.getId())).willReturn(Optional.of(persoon1));
        given(persoonRepository.findById(persoon2.getId())).willReturn(Optional.of(persoon2));
        given(afbeeldingRepository.findById(afbeelding1.getId())).willReturn(Optional.of(afbeelding1));
        given(afbeeldingRepository.findById(afbeelding2.getId())).willReturn(Optional.of(afbeelding2));
        given(afbeeldingRepository.findById(afbeelding3.getId())).willReturn(Optional.of(afbeelding3));
        given(afbeeldingRepository.findById(afbeelding4.getId())).willReturn(Optional.of(afbeelding4));

        given(entityManager.createNativeQuery(anyString())).willReturn(mockQuery);
        given(mockQuery.getResultList()).willReturn(Arrays.asList(
                BigInteger.valueOf(afbeelding1.getId()), BigInteger.valueOf(afbeelding2.getId()),
                BigInteger.valueOf(afbeelding3.getId()), BigInteger.valueOf(afbeelding4.getId())));

        HashMap<String, Set> returnValue = zoekenController.zoeken(persoon1.getId() + ";" + persoon2.getId(), 4, 0, "dateAsc");

        assertThat(returnValue.containsKey("namen"), is(true));
        assertThat(returnValue.containsKey("afbeeldingen"), is(true));

        Set<Persoon> persoonSet = returnValue.get("namen");
        Set<Afbeelding> afbeeldingenSet = returnValue.get("afbeeldingen");

        assertThat(persoonSet, hasSize(2));
        assertThat(persoonSet, hasItem(persoon1));
        assertThat(persoonSet, hasItem(persoon2));
        assertThat(afbeeldingenSet, hasSize(4));

        Iterator<Afbeelding> afbeeldingIterator = afbeeldingenSet.iterator();

        assertThat(afbeeldingIterator.next(), is(afbeelding4));
        assertThat(afbeeldingIterator.next(), is(afbeelding2));
        assertThat(afbeeldingIterator.next(), is(afbeelding1));
        assertThat(afbeeldingIterator.next(), is(afbeelding3));

        returnValue = zoekenController.zoeken(persoon1.getId() + ";" + persoon2.getId(), 4, 0, "dateAsc");

        assertThat(returnValue.containsKey("namen"), is(true));
        assertThat(returnValue.containsKey("afbeeldingen"), is(true));

        persoonSet = returnValue.get("namen");
        afbeeldingenSet = returnValue.get("afbeeldingen");

        assertThat(persoonSet, hasSize(2));
        assertThat(persoonSet, hasItem(persoon1));
        assertThat(persoonSet, hasItem(persoon2));
        assertThat(afbeeldingenSet, hasSize(4));

        afbeeldingIterator = afbeeldingenSet.iterator();

        assertThat(afbeeldingIterator.next(), is(afbeelding4));
        assertThat(afbeeldingIterator.next(), is(afbeelding2));
        assertThat(afbeeldingIterator.next(), is(afbeelding1));
        assertThat(afbeeldingIterator.next(), is(afbeelding3));

        verify(entityManager, times(1)).createNativeQuery(anyString());
    }


    @Test
    public void testZoeken_2Personen2AfbeeldingenPerPersoonSortAsc2AfbeeldingenPerPage() {
        final Persoon persoon1 = newPersoon();
        final Persoon persoon2 = newPersoon();
        final Afbeelding afbeelding1 = newAfbeelding(persoon1);
        final Afbeelding afbeelding2 = newAfbeelding(persoon1);
        final Afbeelding afbeelding3 = newAfbeelding(persoon2);
        final Afbeelding afbeelding4 = newAfbeelding(persoon2);
        final Query mockQuery = mock(Query.class);
        afbeelding1.setOrgineleDatum(LocalDateTime.now().minusDays(10));
        afbeelding2.setOrgineleDatum(LocalDateTime.now().minusDays(80));
        afbeelding3.setOrgineleDatum(LocalDateTime.now().minusDays(1));
        afbeelding4.setOrgineleDatum(LocalDateTime.now().minusDays(99));

        given(persoonRepository.findById(persoon1.getId())).willReturn(Optional.of(persoon1));
        given(persoonRepository.findById(persoon2.getId())).willReturn(Optional.of(persoon2));
        given(afbeeldingRepository.findById(afbeelding1.getId())).willReturn(Optional.of(afbeelding1));
        given(afbeeldingRepository.findById(afbeelding2.getId())).willReturn(Optional.of(afbeelding2));
        given(afbeeldingRepository.findById(afbeelding3.getId())).willReturn(Optional.of(afbeelding3));
        given(afbeeldingRepository.findById(afbeelding4.getId())).willReturn(Optional.of(afbeelding4));

        given(entityManager.createNativeQuery(anyString())).willReturn(mockQuery);
        given(mockQuery.getResultList()).willReturn(Arrays.asList(
                BigInteger.valueOf(afbeelding1.getId()), BigInteger.valueOf(afbeelding2.getId()),
                BigInteger.valueOf(afbeelding3.getId()), BigInteger.valueOf(afbeelding4.getId())));

        final HashMap<String, Set> returnValue1 = zoekenController.zoeken(persoon1.getId() + ";" + persoon2.getId(), 2, 0, "dateAsc");

        assertThat(returnValue1.containsKey("namen"), is(true));
        assertThat(returnValue1.containsKey("afbeeldingen"), is(true));

        final Set<Persoon> persoonSet1 = returnValue1.get("namen");
        final Set<Afbeelding> afbeeldingenSet1 = returnValue1.get("afbeeldingen");

        assertThat(persoonSet1, hasSize(2));
        assertThat(persoonSet1, hasItem(persoon1));
        assertThat(persoonSet1, hasItem(persoon2));
        assertThat(afbeeldingenSet1, hasSize(2));

        final Iterator<Afbeelding> afbeeldingIterator1 = afbeeldingenSet1.iterator();

        assertThat(afbeeldingIterator1.next(), is(afbeelding4));
        assertThat(afbeeldingIterator1.next(), is(afbeelding2));

        final HashMap<String, Set> returnValue2 = zoekenController.zoeken(persoon1.getId() + ";" + persoon2.getId(), 2, 2, "dateAsc");

        assertThat(returnValue2.containsKey("namen"), is(true));
        assertThat(returnValue2.containsKey("afbeeldingen"), is(true));

        final Set<Persoon> persoonSet2 = returnValue2.get("namen");
        final Set<Afbeelding> afbeeldingenSet2 = returnValue2.get("afbeeldingen");

        assertThat(persoonSet2, hasSize(2));
        assertThat(persoonSet2, hasItem(persoon1));
        assertThat(persoonSet2, hasItem(persoon2));
        assertThat(afbeeldingenSet2, hasSize(2));

        final Iterator<Afbeelding> afbeeldingIterator2 = afbeeldingenSet2.iterator();

        assertThat(afbeeldingIterator2.next(), is(afbeelding1));
        assertThat(afbeeldingIterator2.next(), is(afbeelding3));
    }

    @Test
    public void testZoeken_zonderPersonen() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Personen zijn/is verplicht");

        zoekenController.zoeken(null, 1, 1, null);
    }

    @Test
    public void testZoeken_zonderHoeveelheid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Hoeveelheid is verplicht");

        zoekenController.zoeken("1", null, 1, null);
    }

    @Test
    public void testZoeken_zonderStart() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Start is verplicht");

        zoekenController.zoeken("1", 1, null, null);
    }

    @Test
    public void testZoeken_zonderBestaandePersonen() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Er zijn geen personen gevonden met behulp van de volgende zoekgegevens: 1 de zoek gegevens moeten er als volgt uitzien: persoonid1;persoonid2;etc");

        zoekenController.zoeken("1",1,1,null);
    }

}