package nl.robertvankammen.fotoapi.controller;

import nl.robertvankammen.fotoapi.UrnUtils;
import nl.robertvankammen.fotoapi.model.Afbeelding;
import nl.robertvankammen.fotoapi.model.Persoon;
import nl.robertvankammen.fotoapi.repository.AfbeeldingRepository;
import nl.robertvankammen.fotoapi.repository.PersoonRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static nl.robertvankammen.fotoapi.FotoApiApplication.midiSize;
import static nl.robertvankammen.fotoapi.FotoApiApplication.thumbSize;
import static nl.robertvankammen.fotoapi.ObjectBuilder.newAfbeelding;
import static nl.robertvankammen.fotoapi.ObjectBuilder.newPersoon;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class AfbeeldingControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private AfbeeldingRepository afbeeldingRepository;

    @Mock
    private PersoonRepository persoonRepository;

    @Mock
    private UrnUtils urnUtils;

    @Mock
    private Environment environment;

    @InjectMocks
    private AfbeeldingController afbeeldingController;

    @Test
    public void testGetAlleAfbeeldingen() {
        final Afbeelding afbeelding = new Afbeelding();
        given(afbeeldingRepository.findAllByOrderByUrnAsc()).willReturn(Collections.singletonList(afbeelding));

        List<Afbeelding> afbeeldingenLijst = afbeeldingController.getAlleAfbeeldingen();

        assertThat(afbeeldingenLijst, hasSize(1));
        assertThat(afbeeldingenLijst, hasItem(afbeelding));
    }

    @Test
    public void testMaakAfbeelding() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding = newAfbeelding(persoon);
        final Afbeelding databaseAfbeelding = mock(Afbeelding.class);

        persoon.setAfbeeldingenLijst(new ArrayList<>());

        given(afbeeldingRepository.findByUrn(afbeelding.getUrn())).willReturn(Optional.empty());
        given(afbeeldingRepository.save(any())).willReturn(databaseAfbeelding);
        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));

        assertThat(afbeeldingController.maakNieuweAfbeelding(afbeelding), is(databaseAfbeelding));
        assertThat(persoon.getAfbeeldingenLijst(), hasSize(1));
        assertThat(persoon.getAfbeeldingenLijst(), hasItem(databaseAfbeelding));
    }

    @Test
    public void testMaakAfbeelding_metNietBestaandePersoon() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding = newAfbeelding(persoon);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Persoon met id: " + persoon.getId() + " bestaat niet. Deze kan daarom ook niet worden toegevoegd aan de afbeelding");

        given(afbeeldingRepository.findByUrn(afbeelding.getUrn())).willReturn(Optional.empty());

        afbeeldingController.maakNieuweAfbeelding(afbeelding);
    }

    @Test
    public void testMaakAfbeelding_zonderUrn() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("afbeelding.urn moet gevuld zijn");

        final Afbeelding afbeelding = newAfbeelding();
        afbeelding.setUrn(null);

        afbeeldingController.maakNieuweAfbeelding(afbeelding);
    }

    @Test
    public void testMaakAfbeelding_zonderLaatstGewijzigdDatum() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("afbeelding.laatstGewijzigdDatumTijd moet gevuld zijn");

        final Afbeelding afbeelding = newAfbeelding();
        afbeelding.setLaatstGewijzigdDatumTijd(null);

        afbeeldingController.maakNieuweAfbeelding(afbeelding);
    }

    @Test
    public void testMaakAfbeelding_zonderOrgineleDatum() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("afbeelding.orgineleDatum moet gevuld zijn");

        final Afbeelding afbeelding = newAfbeelding();
        afbeelding.setOrgineleDatum(null);

        afbeeldingController.maakNieuweAfbeelding(afbeelding);
    }

    @Test
    public void testMaakAfbeelding_afbeeldingBestaatAl() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding = newAfbeelding(persoon);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Afbeelding met urn: " + afbeelding.getUrn() + " bestaat al, als je deze wilt updaten gebruik de PUT methode");

        given(afbeeldingRepository.findByUrn(afbeelding.getUrn())).willReturn(Optional.of(afbeelding));

        afbeeldingController.maakNieuweAfbeelding(afbeelding);
    }

    @Test
    public void testMaakAfbeelding_metFoutiveUrn() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("urnnn is geen geldige urn, deze moet bestaan uit het volgende urn:extention:subpath:etc:filename");

        given(urnUtils.maakPathVanUrn("urnnn")).willThrow(new IllegalArgumentException("urnnn is geen geldige urn, deze moet bestaan uit het volgende urn:extention:subpath:etc:filename"));

        final Afbeelding afbeelding = newAfbeelding();
        afbeelding.setUrn("urnnn");


        afbeeldingController.maakNieuweAfbeelding(afbeelding);
    }

    @Test
    public void testUpdateAfbeelding() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding = newAfbeelding(persoon);
        final Afbeelding afbeeldingDatabase = newAfbeelding();

        afbeelding.getPersonenLijst().get(0).setAfbeeldingenLijst(new ArrayList<>());
        afbeeldingDatabase.setUrn(afbeelding.getUrn());

        given(afbeeldingRepository.findById(afbeelding.getId())).willReturn(Optional.of(afbeeldingDatabase));
        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));
        given(afbeeldingRepository.save(afbeeldingDatabase)).willReturn(afbeeldingDatabase);

        assertThat(afbeeldingController.updateAfbeelding(afbeelding.getId().toString(), afbeelding), is(afbeeldingDatabase));
        assertThat(afbeeldingDatabase.getPersonenLijst(), hasSize(1));
        assertThat(afbeeldingDatabase.getPersonenLijst(), hasItem(persoon));
        assertThat(persoon.getAfbeeldingenLijst(), hasSize(1));
        assertThat(persoon.getAfbeeldingenLijst(), hasItem(afbeeldingDatabase));
    }

    @Test
    public void testUpdateAfbeelding_nietBestaandeAfbeelding() {
        final Afbeelding afbeelding = newAfbeelding();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Afbeelding met id: " + afbeelding.getId() + " bestaat niet. Gebruik POST methode om een nieuwe afbeelding toe te voegen");

        afbeeldingController.updateAfbeelding(afbeelding.getId().toString(), afbeelding);
    }

    @Test
    public void testUpdateAfbeelding_nieuweUrn() {
        final Afbeelding afbeelding = newAfbeelding();
        final Afbeelding afbeelding2 = newAfbeelding();
        afbeelding2.setUrn("urn:jpg:" + RandomStringUtils.randomAlphabetic(10));

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("De urn van de afbeelding mag niet worden aangepast");

        given(afbeeldingRepository.findById(afbeelding.getId())).willReturn(Optional.of(afbeelding2));

        afbeeldingController.updateAfbeelding(afbeelding.getId().toString(), afbeelding);
    }

    @Test
    public void testVerwijderAfbeelding() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding = newAfbeelding(persoon);

        given(afbeeldingRepository.findById(afbeelding.getId())).willReturn(Optional.of(afbeelding));

        assertThat(afbeeldingController.verwijderAfbeelding(afbeelding.getId().toString()), is("Afbeelding met id: " + afbeelding.getId() + " is verwijderd"));
        assertThat(persoon.getAfbeeldingenLijst(), hasSize(0));
    }

    @Test
    public void testVerwijderAfbeelding_persoonMetMeerdereAfbeeldingen() {
        final Persoon persoon = newPersoon();
        final Afbeelding afbeelding1 = newAfbeelding(persoon);
        final Afbeelding afbeelding2 = newAfbeelding(persoon);

        given(afbeeldingRepository.findById(afbeelding1.getId())).willReturn(Optional.of(afbeelding1));

        assertThat(afbeeldingController.verwijderAfbeelding(afbeelding1.getId().toString()), is("Afbeelding met id: " + afbeelding1.getId() + " is verwijderd"));
        assertThat(persoon.getAfbeeldingenLijst(), hasSize(1));
        assertThat(persoon.getAfbeeldingenLijst(), hasItem(afbeelding2));
    }

    @Test
    public void testGetMaxAfbeelding() throws IOException {
        final Afbeelding afbeelding = newAfbeelding();
        final String path = this.getClass().getResource("testFoto1.jpg").getPath();

        given(environment.getRequiredProperty("basisLocatie")).willReturn(path.replaceFirst("/testFoto1.jpg", ""));
        given(afbeeldingRepository.findById(afbeelding.getId())).willReturn(Optional.of(afbeelding));
        given(urnUtils.maakPathVanUrn(afbeelding.getUrn())).willReturn("testFoto1.jpg");

        assertThat(afbeeldingController.getMaxAfbeelding(afbeelding.getId().toString()), is(Files.readAllBytes(Paths.get(path))));
    }

    @Test
    public void testGetMidiAfbeelding() throws IOException {
        final Afbeelding afbeelding = newAfbeelding();
        final String path = this.getClass().getResource(midiSize + "/testFotoMidi.jpg").getPath();

        given(environment.getRequiredProperty("basisLocatie")).willReturn(path.replaceFirst("/" + midiSize + "/testFotoMidi.jpg", ""));
        given(afbeeldingRepository.findById(afbeelding.getId())).willReturn(Optional.of(afbeelding));
        given(urnUtils.maakPathVanUrn(afbeelding.getUrn())).willReturn("testFotoMidi.jpg");

        assertThat(afbeeldingController.getMidiAfbeelding(afbeelding.getId().toString()), is(Files.readAllBytes(Paths.get(path))));
    }

    @Test
    public void testGetThumbAfbeelding() throws IOException {
        final Afbeelding afbeelding = newAfbeelding();
        final String path = this.getClass().getResource(thumbSize + "/testFotoThumb.jpg").getPath();

        given(environment.getRequiredProperty("basisLocatie")).willReturn(path.replaceFirst("/" + thumbSize + "/testFotoThumb.jpg", ""));
        given(afbeeldingRepository.findById(afbeelding.getId())).willReturn(Optional.of(afbeelding));
        given(urnUtils.maakPathVanUrn(afbeelding.getUrn())).willReturn("testFotoThumb.jpg");

        assertThat(afbeeldingController.getThumbAfbeelding(afbeelding.getId().toString()), is(Files.readAllBytes(Paths.get(path))));
    }
}