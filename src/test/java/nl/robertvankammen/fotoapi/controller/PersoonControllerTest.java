package nl.robertvankammen.fotoapi.controller;

import nl.robertvankammen.fotoapi.model.Persoon;
import nl.robertvankammen.fotoapi.repository.PersoonRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockMultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static nl.robertvankammen.fotoapi.ObjectBuilder.newPersoon;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PersoonControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Mock
    private PersoonRepository persoonRepository;

    @Mock
    private Environment environment;

    @InjectMocks
    private PersoonController persoonController;

    @Test
    public void testGetAllePersonen() {
        final Persoon persoon = newPersoon();
        final List<Persoon> personenLijst = Collections.singletonList(persoon);

        given(persoonRepository.findAll()).willReturn(personenLijst);

        List<Persoon> personenLijstVanController = persoonController.getAllePersonen();
        assertThat(personenLijstVanController, hasSize(1));
        assertThat(personenLijstVanController, hasItem(persoon));
    }

    @Test
    public void testMaakNieuwPersoon() {
        final Persoon persoon = newPersoon();
        final Persoon databasePersoon = newPersoon();

        given(persoonRepository.findByNaam(persoon.getNaam())).willReturn(Optional.empty());
        given(persoonRepository.save(persoon)).willReturn(databasePersoon);

        assertThat(persoonController.maakNieuwPersoon(persoon), is(databasePersoon));
    }

    @Test
    public void testMaakPersoon_zonderNaam() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("persoon.naam is verplicht");

        final Persoon persoon = newPersoon();
        persoon.setNaam(null);

        persoonController.maakNieuwPersoon(persoon);
    }

    @Test
    public void testMaakPersoon_persoonBestaatAl() {
        final Persoon persoon = newPersoon();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Persoon met naam: " + persoon.getNaam() + " bestaat al. Deze kan niet nog een keer gemaakt worden");

        given(persoonRepository.findByNaam(persoon.getNaam())).willReturn(Optional.of(persoon));

        persoonController.maakNieuwPersoon(persoon);
    }

    @Test
    public void testGetPersoon() {
        final Persoon persoon = newPersoon();

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));

        assertThat(persoonController.getPersoonBijId(persoon.getId().toString()), is(persoon));
    }

    @Test
    public void testGetPersoon_bestaatNiet() {
        final Long id = 1L;

        given(persoonRepository.findById(id)).willReturn(Optional.empty());

        assertThat(persoonController.getPersoonBijId(id.toString()), is(nullValue()));
    }

    @Test
    public void testGetProfielFoto() throws IOException {
        final Persoon persoon = newPersoon();
        persoon.setProfielFoto(true);
        persoon.setId(1234L);
        final String path = this.getClass().getResource("profielfotos/1234.jpg").getPath();

        given(environment.getRequiredProperty("basisLocatie")).willReturn(path.replaceFirst("/profielfotos/1234.jpg", ""));
        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));


        assertThat(persoonController.getProfielfoto(persoon.getId().toString()), is(Files.readAllBytes(Paths.get(path))));
    }

    @Test
    public void testGetProfielFoto_persoonBestaatNiet() throws IOException {
        final Persoon persoon = newPersoon();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Persoon met id: " + persoon.getId() + " bestaat niet");

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.empty());

        persoonController.getProfielfoto(persoon.getId().toString());
    }

    @Test
    public void testGetProfielFoto_persoonGeenProfielFoto() throws IOException {
        final Persoon persoon = newPersoon();

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Persoon met id: " + persoon.getId() + " heeft nog geen profiel foto. Voeg deze toe door de PUT methode te gebruiken");

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));

        persoonController.getProfielfoto(persoon.getId().toString());
    }

    @Test
    public void testUpdateProfielFoto() throws IOException {
        final Long newId = 9876L;
        final String path = this.getClass().getResource("profielfotos/1234.jpg").getPath();
        final String pathProfielFoto = path.replaceFirst("1234.jpg", newId + ".jpg");
        final Persoon persoon = newPersoon();
        persoon.setId(newId);
        FileInputStream inputFile = new FileInputStream(path);
        MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);

        given(persoonRepository.findById(persoon.getId())).willReturn(Optional.of(persoon));
        given(environment.getRequiredProperty("basisLocatie")).willReturn(path.replaceFirst("/profielfotos/1234.jpg", ""));


        assertThat(persoonController.updateProfielFoto(persoon.getId().toString(), file), is("Profiel foto van " + persoon.getNaam() + " is geupdated"));
        assertThat(Files.readAllBytes(Paths.get(path)), is(Files.readAllBytes(Paths.get(pathProfielFoto))));
        assertThat(persoon.getProfielFoto(), is(true));

        Files.delete(Paths.get(pathProfielFoto));
    }
}