package nl.robertvankammen.fotoapi.controller;

import nl.robertvankammen.fotoapi.UrnUtils;
import nl.robertvankammen.fotoapi.model.Afbeelding;
import nl.robertvankammen.fotoapi.model.Persoon;
import nl.robertvankammen.fotoapi.repository.AfbeeldingRepository;
import nl.robertvankammen.fotoapi.repository.PersoonRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static nl.robertvankammen.fotoapi.FotoApiApplication.midiSize;
import static nl.robertvankammen.fotoapi.FotoApiApplication.thumbSize;

@RestController
public class AfbeeldingController {

    private final AfbeeldingRepository afbeeldingRepository;
    private final PersoonRepository persoonRepository;
    private final UrnUtils urnUtils;
    private final Environment environment;

    public AfbeeldingController(AfbeeldingRepository afbeeldingRepository, PersoonRepository persoonRepository, UrnUtils urnUtils, Environment environment) {
        this.afbeeldingRepository = afbeeldingRepository;
        this.persoonRepository = persoonRepository;
        this.urnUtils = urnUtils;
        this.environment = environment;
    }

    @RequestMapping("/afbeeldingen")
    public List<Afbeelding> getAlleAfbeeldingen() {
        return afbeeldingRepository.findAllByOrderByUrnAsc();
    }

    @PostMapping("/afbeeldingen")
    public Afbeelding maakNieuweAfbeelding(@RequestBody Afbeelding afbeelding) {
        if (StringUtils.isBlank(afbeelding.getUrn())) {
            throw new IllegalArgumentException("afbeelding.urn moet gevuld zijn");
        }
        urnUtils.maakPathVanUrn(afbeelding.getUrn());

        if (afbeelding.getLaatstGewijzigdDatumTijd() == null) {
            throw new IllegalArgumentException("afbeelding.laatstGewijzigdDatumTijd moet gevuld zijn");
        }
        if (afbeelding.getOrgineleDatum() == null) {
            throw new IllegalArgumentException("afbeelding.orgineleDatum moet gevuld zijn");
        }

        final Optional<Afbeelding> optionalAfbeelding = afbeeldingRepository.findByUrn(afbeelding.getUrn());
        if (optionalAfbeelding.isPresent()) {
            throw new IllegalArgumentException("Afbeelding met urn: " + afbeelding.getUrn() + " bestaat al, als je deze wilt updaten gebruik de PUT methode");
        }
        final Afbeelding nieuweAfbeelding = afbeeldingRepository.save(afbeelding);
        afbeelding.getPersonenLijst().forEach(p -> getPersoonVanId(p.getId()).getAfbeeldingenLijst().add(nieuweAfbeelding));

        return afbeeldingRepository.save(nieuweAfbeelding);
    }

    @PutMapping("/afbeeldingen/{id}")
    public Afbeelding updateAfbeelding(@PathVariable("id") String id, @RequestBody Afbeelding afbeelding) {
        final Afbeelding afbeeldingDatabase = getAfbeeldingVanId(id);
        if (!afbeeldingDatabase.getUrn().equals(afbeelding.getUrn())) {
            throw new IllegalArgumentException("De urn van de afbeelding mag niet worden aangepast!");
        }
        cleanAfbeeldingPersoon(afbeelding);
        afbeeldingDatabase.getPersonenLijst().forEach(p -> p.getAfbeeldingenLijst().remove(afbeeldingDatabase));
        afbeeldingDatabase.setPersonenLijst(new ArrayList<>());
        afbeelding.getPersonenLijst().forEach(p -> afbeeldingDatabase.getPersonenLijst().add(getPersoonVanId(p.getId())));
        afbeeldingDatabase.getPersonenLijst().forEach(p -> p.getAfbeeldingenLijst().add(afbeeldingDatabase));

        afbeeldingDatabase.setLaatstGewijzigdDatumTijd(afbeelding.getLaatstGewijzigdDatumTijd());
        afbeeldingDatabase.setOrgineleDatum(afbeelding.getOrgineleDatum());
        return afbeeldingRepository.save(afbeeldingDatabase);
    }

    @DeleteMapping("/afbeeldingen/{id}")
    public String verwijderAfbeelding(@PathVariable("id") String id) {
        Afbeelding afbeelding = getAfbeeldingVanId(id);
        afbeelding.getPersonenLijst().forEach(persoon -> persoon.getAfbeeldingenLijst().remove(afbeelding));
        afbeeldingRepository.delete(afbeelding);
        return "Afbeelding met id: " + id + " is verwijderd";
    }

    @RequestMapping(value = "/afbeeldingen/{id}/max", produces = "image/jpg")
    public byte[] getMaxAfbeelding(@PathVariable("id") String id) throws IOException {
        return getAfbeeldingVoorGrote(id, "");
    }

    @RequestMapping(value = "/afbeeldingen/{id}/midi", produces = "image/jpg")
    public byte[] getMidiAfbeelding(@PathVariable("id") String id) throws IOException {
        return getAfbeeldingVoorGrote(id, midiSize.toString());
    }

    @RequestMapping(value = "/afbeeldingen/{id}/thumb", produces = "image/jpg")
    public byte[] getThumbAfbeelding(@PathVariable("id") String id) throws IOException {
        return getAfbeeldingVoorGrote(id, thumbSize.toString());
    }

    private Afbeelding getAfbeeldingVanId(String id) {
        final Optional<Afbeelding> optionalAfbeelding = afbeeldingRepository.findById(Long.parseLong(id));
        if (optionalAfbeelding.isPresent()) {
            return optionalAfbeelding.get();
        } else {
            throw new IllegalArgumentException("Afbeelding met id: " + id + " bestaat niet. Gebruik POST methode om een nieuwe afbeelding toe te voegen");
        }
    }

    private Persoon getPersoonVanId(Long id) {
        final Optional<Persoon> optionalPersoon = persoonRepository.findById(id);
        if (optionalPersoon.isPresent()) {
            return optionalPersoon.get();
        } else {
            throw new IllegalArgumentException("Persoon met id: " + id + " bestaat niet. Deze kan daarom ook niet worden toegevoegd aan de afbeelding");
        }
    }

    private byte[] getAfbeeldingVoorGrote(String id, String grote) throws IOException {
        final Afbeelding afbeelding = getAfbeeldingVanId(id);
        final String basisLocatieMetGrote;

        if (StringUtils.isNotBlank(grote)) {
            basisLocatieMetGrote = environment.getRequiredProperty("basisLocatie").concat(File.separator).concat(grote);
        } else {
            basisLocatieMetGrote = environment.getRequiredProperty("basisLocatie");
        }

        return Files.readAllBytes(Paths.get(basisLocatieMetGrote.concat(File.separator).concat(urnUtils.maakPathVanUrn(afbeelding.getUrn()))));
    }

    private void cleanAfbeeldingPersoon(Afbeelding afbeelding) {
        afbeelding.getPersonenLijst().forEach(persoon -> persoon.setAfbeeldingenLijst(new ArrayList<>()));
    }
}
