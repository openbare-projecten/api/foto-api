package nl.robertvankammen.fotoapi.controller;

import nl.robertvankammen.fotoapi.model.Afbeelding;
import nl.robertvankammen.fotoapi.model.AfbeeldingLijstCache;
import nl.robertvankammen.fotoapi.model.Persoon;
import nl.robertvankammen.fotoapi.repository.AfbeeldingRepository;
import nl.robertvankammen.fotoapi.repository.PersoonRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ZoekenController {

    private final PersoonRepository persoonRepository;
    private final HashMap<String, AfbeeldingLijstCache> afbeeldingenCacheMap;
    private final AfbeeldingRepository afbeeldingRepository;
    private final EntityManager em;
    private final String schema;

    public ZoekenController(PersoonRepository persoonRepository, AfbeeldingRepository afbeeldingRepository, EntityManager em, Environment environment) {
        this.persoonRepository = persoonRepository;
        this.afbeeldingRepository = afbeeldingRepository;
        this.em = em;
        this.schema = environment.getRequiredProperty("spring.jpa.properties.hibernate.default_schema");
        this.afbeeldingenCacheMap = new HashMap<>();
    }

    @RequestMapping("zoeken")
    public HashMap<String, Set> zoeken(@RequestParam(value = "personen") String personen,
                                       @RequestParam(value = "hoeveelheid") Integer hoeveelheid,
                                       @RequestParam(value = "start") Integer start,
                                       @RequestParam(value = "sorteren", required = false) String sorteren) {
        if (StringUtils.isBlank(personen)) {
            throw new IllegalArgumentException("Personen zijn/is verplicht");
        }
        if (hoeveelheid == null) {
            throw new IllegalArgumentException("Hoeveelheid is verplicht");
        }
        if (start == null) {
            throw new IllegalArgumentException("Start is verplicht");
        }
        final HashMap<String, Set> returnWaarde = new HashMap<>();

        final Set<Persoon> personenLijst = getPersonenVanIds(personen);
        if (personenLijst.size() == 0) {
            throw new IllegalArgumentException("Er zijn geen personen gevonden met behulp van de volgende zoekgegevens: " + personen + " de zoek gegevens moeten er als volgt uitzien: persoonid1;persoonid2;etc");
        }

        LinkedHashSet<Afbeelding> afbeeldingenLijst = getAfbeeldingen(personen, personenLijst);
        afbeeldingenLijst = sorteerAfbeeldingen(afbeeldingenLijst, sorteren);
        afbeeldingenLijst = pagineringAfbeeldingen(afbeeldingenLijst, start, hoeveelheid);

        returnWaarde.put("namen", personenLijst);
        returnWaarde.put("afbeeldingen", afbeeldingenLijst);
        return returnWaarde;

    }

    private LinkedHashSet<Afbeelding> getAfbeeldingen(String personen, Set<Persoon> personenLijst) {
        final LinkedHashSet<Afbeelding> afbeeldingenList;
        if (afbeeldingenCacheMap.containsKey(personen)) {
            AfbeeldingLijstCache afbeeldingLijstCache = afbeeldingenCacheMap.get(personen);
            if (LocalDateTime.now().isBefore(afbeeldingLijstCache.getVervalDatum())) {
                return afbeeldingLijstCache.getAfbeeldingenLijst();
            } else {
                afbeeldingenCacheMap.remove(personen);
            }
        }
        afbeeldingenList = getGezamelijkeAfbeeldingen(personenLijst);
        afbeeldingenCacheMap.put(personen, new AfbeeldingLijstCache(personen, afbeeldingenList));
        return afbeeldingenList;
    }

    private Set<Persoon> getPersonenVanIds(String personen) {
        return Stream.of(personen.split(";"))
                .map(id -> persoonRepository.findById(Long.parseLong(id)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private LinkedHashSet<Afbeelding> getGezamelijkeAfbeeldingen(Set<Persoon> personenLijst) {
        if (personenLijst.size() == 1) {
            return new LinkedHashSet<>(personenLijst.iterator().next().getAfbeeldingenLijst());
        } else {
            String query = personenLijst.stream()
                    .map(p -> "SELECT afbeelding_id FROM " + schema + ".afbeelding_personen_lijst WHERE personen_lijst_id = " + p.getId())
                    .collect(Collectors.joining(" Intersect "));
            List<BigInteger> afbeeldingenIds = em.createNativeQuery(query).getResultList();
            return afbeeldingenIds.stream()
                    .map(BigInteger::longValue)
                    .map(afbeeldingRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toCollection(LinkedHashSet::new));
        }
    }

    private LinkedHashSet<Afbeelding> sorteerAfbeeldingen(LinkedHashSet<Afbeelding> afbeeldingenLijst, String sorteren) {
        if (StringUtils.isBlank(sorteren))
            return afbeeldingenLijst;
        switch (sorteren) {
            case "dateAsc":
                return afbeeldingenLijst.stream().sorted(Comparator.comparing(Afbeelding::getOrgineleDatum)).collect(Collectors.toCollection(LinkedHashSet::new));
            case "dateDesc":
                return afbeeldingenLijst.stream().sorted((o1, o2) -> o2.getOrgineleDatum().compareTo(o1.getOrgineleDatum())).collect(Collectors.toCollection(LinkedHashSet::new));
            default:
                return afbeeldingenLijst;
        }
    }

    private LinkedHashSet<Afbeelding> pagineringAfbeeldingen(LinkedHashSet<Afbeelding> afbeeldingenLijst, Integer start, Integer hoeveelheid) {
        if (start != null && start >= 0 && hoeveelheid != null && hoeveelheid > 0) {
            LinkedHashSet<Afbeelding> returnPictures = new LinkedHashSet<>();
            Iterator<Afbeelding> itr = afbeeldingenLijst.iterator();
            for (int i = 0; itr.hasNext(); i++) {
                Afbeelding p = itr.next();
                if (i >= start && start + hoeveelheid > i) {
                    returnPictures.add(p);
                }
            }
            return returnPictures;
        }
        return afbeeldingenLijst;
    }

}
