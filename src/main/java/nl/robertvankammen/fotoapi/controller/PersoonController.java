package nl.robertvankammen.fotoapi.controller;

import nl.robertvankammen.fotoapi.model.Persoon;
import nl.robertvankammen.fotoapi.repository.PersoonRepository;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
public class PersoonController {

    private final PersoonRepository persoonRepository;
    private final Environment environment;

    public PersoonController(PersoonRepository persoonRepository, Environment environment) {
        this.persoonRepository = persoonRepository;
        this.environment = environment;
    }

    @RequestMapping("/personen")
    public List<Persoon> getAllePersonen() {
        return persoonRepository.findAll();
    }

    @PostMapping("/personen")
    public Persoon maakNieuwPersoon(@RequestBody Persoon persoon) {
        if (StringUtils.isBlank(persoon.getNaam())) {
            throw new IllegalArgumentException("persoon.naam is verplicht");
        }
        if (persoonRepository.findByNaam(persoon.getNaam()).isPresent()) {
            throw new IllegalArgumentException("Persoon met naam: " + persoon.getNaam() + " bestaat al. Deze kan niet nog een keer gemaakt worden");
        }
        return persoonRepository.save(persoon);
    }

    @RequestMapping("/personen/bijNaam/{naam}")
    public Persoon getPersoonBijNaam(@PathVariable("naam") String naam) {
        return persoonRepository.findByNaam(naam).orElse(null);
    }

    @RequestMapping("/personen/{id}")
    public Persoon getPersoonBijId(@PathVariable("id") String id) {
        return persoonRepository.findById(Long.parseLong(id)).orElse(null);
    }

    @RequestMapping(value = "/personen/{id}/profielfoto", produces = "image/jpg")
    public byte[] getProfielfoto(@PathVariable("id") String id) throws IOException {
        final Persoon persoon = getPersoonVanId(id);
        if (!BooleanUtils.toBooleanDefaultIfNull(persoon.getProfielFoto(), false)) {
            throw new IllegalArgumentException("Persoon met id: " + id + " heeft nog geen profiel foto. Voeg deze toe door de PUT methode te gebruiken");
        }
        return Files.readAllBytes(Paths.get(environment.getRequiredProperty("basisLocatie") + "/profielfotos/" + persoon.getId() + ".jpg"));
    }

    @PutMapping("/personen/{id}/profielfoto")
    public String updateProfielFoto(@PathVariable("id") String id, @RequestParam MultipartFile file) throws IOException {
        final Persoon persoon = getPersoonVanId(id);

        final File outFile = new File(environment.getRequiredProperty("basisLocatie") + "/profielfotos/" + persoon.getId() + ".jpg");
        outFile.getParentFile().mkdirs();
        outFile.createNewFile();

        final FileOutputStream fos = new FileOutputStream(outFile);
        fos.write(file.getBytes());
        fos.close();

        persoon.setProfielFoto(true);
        persoonRepository.save(persoon);

        return "Profiel foto van " + persoon.getNaam() + " is geupdated";
    }

    private Persoon getPersoonVanId(String id) {
        final Optional<Persoon> optionalPersoon = persoonRepository.findById(Long.parseLong(id));
        if (optionalPersoon.isPresent()) {
            return optionalPersoon.get();
        } else {
            throw new IllegalArgumentException("Persoon met id: " + id + " bestaat niet");
        }
    }
}
