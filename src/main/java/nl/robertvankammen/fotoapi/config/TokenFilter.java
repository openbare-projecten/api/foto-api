package nl.robertvankammen.fotoapi.config;

import nl.robertvankammen.fotoapi.model.Afbeelding;
import nl.robertvankammen.fotoapi.model.Token;
import nl.robertvankammen.fotoapi.repository.AfbeeldingRepository;
import nl.robertvankammen.fotoapi.repository.TokenRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.split;

@Component
public class TokenFilter extends OncePerRequestFilter {

    private final TokenRepository tokenRepository;
    private final AfbeeldingRepository afbeeldingRepository;

    public TokenFilter(TokenRepository tokenRepository, AfbeeldingRepository afbeeldingRepository) {
        this.tokenRepository = tokenRepository;
        this.afbeeldingRepository = afbeeldingRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String secretToken = httpServletRequest.getHeader("secretToken");
        // todo dit doet niks, aangezien dit alleen het ip van het cluster returnt
        //System.out.println(httpServletRequest.getRemoteAddr());

        if (isNotBlank(secretToken)) {
            Optional<Token> tokenOptional = tokenRepository.getTokenByTokenIs(secretToken);
            if (tokenOptional.isPresent()) {
                final Token token = tokenOptional.get();
                final String uri = httpServletRequest.getRequestURI();
                if (uri.contains("zoeken")) {
                    final String[] persoonenId = split(httpServletRequest.getParameter("personen"), ";");
                    if (persoonenId.length == 1) {
                        if (Long.parseLong(persoonenId[0]) == token.getPersoon().getId()) {
                            SecurityContextHolder.getContext().setAuthentication(
                                    new UsernamePasswordAuthenticationToken("zoeken", "persoon", Collections.emptyList()));
                        }
                    }
                } else if (uri.contains("afbeeldingen/")) {
                    final String[] uriGedeelte = split(uri, "/");
                    final String id = uriGedeelte[uriGedeelte.length - 2];
                    final Optional<Afbeelding> optionalAfbeelding = afbeeldingRepository.findById(Long.parseLong(id));
                    if (optionalAfbeelding.isPresent()) {
                        // todo dit fixen org.hibernate.LazyInitializationException: failed to lazily initialize a collection of role: nl.robertvankammen.fotoapi.model.Afbeelding.personenLijst, could not initialize proxy - no Session
                        //if(optionalAfbeelding.get().getPersonenLijst().contains(token.getPersoon())){
                        SecurityContextHolder.getContext().setAuthentication(
                                new UsernamePasswordAuthenticationToken("afbeelding", "persoon", Collections.emptyList()));
                        //}
                    }
                }
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
