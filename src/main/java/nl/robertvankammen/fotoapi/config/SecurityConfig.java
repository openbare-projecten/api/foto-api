package nl.robertvankammen.fotoapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static org.apache.commons.lang3.StringUtils.isBlank;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    private final TokenFilter tokenFilter;
    private final Environment environment;

    @Value("${gebruikersnaam:}")
    private String gebruikersnaam;

    @Value("${wachtwoord:}")
    private String wachtwoord;

    public SecurityConfig(TokenFilter tokenFilter, Environment environment) {
        this.tokenFilter = tokenFilter;
        this.environment = environment;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
            }
        };
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().authenticated().and()
                .addFilterBefore(tokenFilter, BasicAuthenticationFilter.class)
                .httpBasic().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        String gebruiker = gebruikersnaam;
        String wachtwoord = this.wachtwoord;

        if (isBlank(gebruiker)) {
            gebruiker = environment.getProperty("spring.security.user.name");
        }
        if (isBlank(wachtwoord)) {
            wachtwoord = environment.getProperty("spring.security.user.password");
        }

        auth
                .inMemoryAuthentication()
                .withUser(gebruiker).password("{noop}" + wachtwoord).roles("USER");
    }
}
