package nl.robertvankammen.fotoapi;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class UrnUtils {

    public String maakPathVanUrn(String urn) {
        String[] subString = urn.split(":");
        if (subString.length >= 3 && subString[0].equals("urn")) {
            String extention = subString[1];
            if (!FotoApiApplication.extentionList.contains(extention.toLowerCase())) {
                throw new IllegalArgumentException("De volgende extentie wordt niet ondersteund: " + extention);
            }
            String restPath = "";
            for (int i = 2; i < subString.length; i++) {
                restPath = restPath.concat(File.separator).concat(subString[i]);
            }
            return restPath.concat(".").concat(extention);
        }
        throw new IllegalArgumentException(urn + " is geen geldige urn, deze moet bestaan uit het volgende urn:extention:subpath:etc:filename");
    }
}
