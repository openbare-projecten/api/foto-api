package nl.robertvankammen.fotoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
@EnableTransactionManagement
public class FotoApiApplication {

    static ArrayList<String> extentionList = new ArrayList<>(Arrays.asList("jpg", "png", "jpeg"));
    public static Integer thumbSize = 256;
    public static Integer midiSize = 720;

    public static void main(String[] args) {
        SpringApplication.run(FotoApiApplication.class, args);
    }
    // todo iets doen met connection pool?
    // todo basis locatie aanpassen middel args
}
