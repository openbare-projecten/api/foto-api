package nl.robertvankammen.fotoapi.repository;

import nl.robertvankammen.fotoapi.model.Persoon;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersoonRepository extends CrudRepository<Persoon, Long> {
    public Optional<Persoon> findByNaam(String naam);

    public List<Persoon> findAll();
}
