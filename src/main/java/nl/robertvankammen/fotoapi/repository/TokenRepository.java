package nl.robertvankammen.fotoapi.repository;

import nl.robertvankammen.fotoapi.model.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends CrudRepository<Token, Long> {
    Optional<Token> getTokenByTokenIs(String token);
}
