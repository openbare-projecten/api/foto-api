package nl.robertvankammen.fotoapi.repository;

import nl.robertvankammen.fotoapi.model.Afbeelding;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AfbeeldingRepository extends CrudRepository<Afbeelding, Long> {
    public List<Afbeelding> findAllByOrderByUrnAsc();

    public Optional<Afbeelding> findByUrn(String urn);
}
