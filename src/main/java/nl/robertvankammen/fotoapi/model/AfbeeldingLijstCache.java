package nl.robertvankammen.fotoapi.model;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;

public class AfbeeldingLijstCache {

    private String zoekTerm;
    private LocalDateTime vervalDatum;
    private LinkedHashSet<Afbeelding> afbeeldingenLijst;

    public AfbeeldingLijstCache(String zoekTerm, LinkedHashSet<Afbeelding> afbeeldingenLijst) {
        this.zoekTerm = zoekTerm;
        this.afbeeldingenLijst = afbeeldingenLijst;
        this.vervalDatum = LocalDateTime.now().plusWeeks(1);
    }

    public String getZoekTerm() {
        return zoekTerm;
    }

    public LocalDateTime getVervalDatum() {
        return vervalDatum;
    }

    public LinkedHashSet<Afbeelding> getAfbeeldingenLijst() {
        return afbeeldingenLijst;
    }
}
