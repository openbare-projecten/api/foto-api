package nl.robertvankammen.fotoapi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Entity
public class Token {

    @Id
    @GeneratedValue
    private Long id;
    private String token;
    private LocalDateTime verloopDatum;
    @OneToOne
    private Persoon persoon;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Persoon getPersoon() {
        return persoon;
    }

    public void setPersoon(Persoon persoon) {
        this.persoon = persoon;
    }

    public LocalDateTime getVerloopDatum() {
        return verloopDatum;
    }

    public void setVerloopDatum(LocalDateTime verloopDatum) {
        this.verloopDatum = verloopDatum;
    }

    public Long getId() {
        return id;
    }
}
