package nl.robertvankammen.fotoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(indexes = {
        @Index(name = "urnIndex", columnList = "urn")
})
public class Afbeelding {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, nullable = false)
    private String urn;
    @Column(nullable = false)
    private LocalDateTime laatstGewijzigdDatumTijd;
    @Column(nullable = false)
    private LocalDateTime orgineleDatum;
    @ManyToMany
    private List<Persoon> personenLijst;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrn() {
        return urn;
    }

    public void setUrn(String urn) {
        this.urn = urn;
    }

    public LocalDateTime getLaatstGewijzigdDatumTijd() {
        return laatstGewijzigdDatumTijd;
    }

    public void setLaatstGewijzigdDatumTijd(LocalDateTime laatstGewijzigdDatumTijd) {
        this.laatstGewijzigdDatumTijd = laatstGewijzigdDatumTijd;
    }

    @JsonIgnore
    public List<Persoon> getPersonenLijst() {
        if (personenLijst == null) {
            personenLijst = new ArrayList<>();
        }
        return personenLijst;
    }

    @JsonProperty
    public void setPersonenLijst(List<Persoon> personenLijst) {
        this.personenLijst = personenLijst;
    }

    public LocalDateTime getOrgineleDatum() {
        return orgineleDatum;
    }

    public void setOrgineleDatum(LocalDateTime orgineleDatum) {
        this.orgineleDatum = orgineleDatum;
    }
}
