package nl.robertvankammen.fotoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Persoon {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, nullable = false)
    private String naam;
    private Boolean profielFoto;
    @JsonIgnore
    @ManyToMany
    private List<Afbeelding> afbeeldingenLijst;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        if (!StringUtils.isBlank(naam)) {
            return naam.trim();
        }
        return null;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public List<Afbeelding> getAfbeeldingenLijst() {
        if (afbeeldingenLijst == null) {
            afbeeldingenLijst = new ArrayList<>();
        }
        return afbeeldingenLijst;
    }

    public void setAfbeeldingenLijst(List<Afbeelding> afbeeldingenLijst) {
        this.afbeeldingenLijst = afbeeldingenLijst;
    }

    public Boolean getProfielFoto() {
        return profielFoto;
    }

    public void setProfielFoto(Boolean profielFoto) {
        this.profielFoto = profielFoto;
    }
}